#!/bin/bash

set -e

DSA_DIR=$(realpath $(dirname "$0"))

CIV_DIR="${HOME}/.steam/steam/steamapps/common/Sid Meier's Civilization VI"
CIV_BIN64="${CIV_DIR}/Civ6Sub"
CIV_LIB64="${CIV_DIR}"

# Required by DSA
export SteamAppId="289070"
export SteamGameId="289070"
export DSA_ORIG_STEAM_API_LIB="${CIV_LIB64}/libsteam_api.so"

. "${DSA_DIR}/common.sh"
. "${DSA_DIR}/config.sh"

echo -e "\033[1;92mLaunching Civilization VI with DummySteamAPI...\033[0m"

cd "${CIV_DIR}"

export LD_LIBRARY_PATH="${CIV_LIB64}:${LD_LIBRARY_PATH}"
export LD_PRELOAD="${DSA_DIR}/bin/libsteam_api.so:${LD_PRELOAD}"

exec "${CIV_BIN64}"
