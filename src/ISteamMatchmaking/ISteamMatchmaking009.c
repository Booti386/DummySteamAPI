#include "debug.h"

#include "ISteamMatchmaking009.h"
#include "ISteamMatchmaking_priv.h"

static const struct ISteamMatchmaking009Vtbl ISteamMatchmaking009_vtbl = {
	ISteamMatchmaking_GetFavoriteGameCount,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	ISteamMatchmaking_RequestLobbyList003,
	ISteamMatchmaking_AddRequestLobbyListStringFilter,
	ISteamMatchmaking_AddRequestLobbyListNumericalFilter,
	ISteamMatchmaking_AddRequestLobbyListNearValueFilter,
	ISteamMatchmaking_AddRequestLobbyListFilterSlotsAvailable,
	ISteamMatchmaking_AddRequestLobbyListDistanceFilter,
	ISteamMatchmaking_AddRequestLobbyListResultCountFilter,
	ISteamMatchmaking_AddRequestLobbyListCompatibleMembersFilter,
	DBG_TRAP,
	ISteamMatchmaking_CreateLobby007,
	DBG_TRAP,
	ISteamMatchmaking_LeaveLobby,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP
};

struct ISteamMatchmaking *SteamMatchmaking009(void)
{
	static struct ISteamMatchmakingImpl impl;

	impl.base.vtbl.v009 = &ISteamMatchmaking009_vtbl;

	return &impl.base;
}
