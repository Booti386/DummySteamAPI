#include "debug.h"

#include "ISteamMatchmaking001.h"
#include "ISteamMatchmaking_priv.h"

static const struct ISteamMatchmaking001Vtbl ISteamMatchmaking001_vtbl = {
	ISteamMatchmaking_GetFavoriteGameCount,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	ISteamMatchmaking_LeaveLobby,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP
};

struct ISteamMatchmaking *SteamMatchmaking001(void)
{
	static struct ISteamMatchmakingImpl impl;

	impl.base.vtbl.v001 = &ISteamMatchmaking001_vtbl;

	return &impl.base;
}
