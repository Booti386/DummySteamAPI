#include <string.h>

#include "CCallback.h"
#include "callbacks.h"
#include "debug.h"
#include "steam.h"

#include "ISteamFriends.h"
#include "ISteamFriends_priv.h"
#include "ISteamFriends001.h"
#include "ISteamFriends013.h"
#include "ISteamFriends014.h"
#include "ISteamFriends015.h"
#include "ISteamFriends017.h"

static const char *steam_friends_version = NULL;

MEMBER const char *ISteamFriends_GetPersonaName(struct ISteamFriends *iface)
{
	struct ISteamFriendsImpl *This = impl_from_ISteamFriends(iface);

	LOG_ENTER_NOTIMPL("(This = %p)", VOIDPTR(This));

	return "Me";
}

EXPORT const char *SteamAPI_ISteamFriends_GetPersonaName(struct ISteamFriends *iface)
{
	return ISteamFriends_GetPersonaName(iface);
}

MEMBER steam_api_call_t ISteamFriends_SetPersonaName(struct ISteamFriends *iface, const char *name)
{
	struct ISteamFriendsImpl *This = impl_from_ISteamFriends(iface);

	LOG_ENTER_NOTIMPL("(This = %p, name = \"%s\")", VOIDPTR(This), debug_str(name));

	return 0;
}

MEMBER enum steam_friends_persona_state ISteamFriends_GetPersonaState(struct ISteamFriends *iface)
{
	struct ISteamFriendsImpl *This = impl_from_ISteamFriends(iface);

	LOG_ENTER_NOTIMPL("(This = %p)", VOIDPTR(This));

	return STEAM_FRIENDS_PERSONA_STATE_ONLINE;
}

MEMBER int ISteamFriends_GetFriendCount(struct ISteamFriends *iface, int flags)
{
	struct ISteamFriendsImpl *This = impl_from_ISteamFriends(iface);

	LOG_ENTER_NOTIMPL("(This = %p, flags = %#x)", VOIDPTR(This), flags);

	return 0;
}

MEMBER enum steam_friends_friend_relationship ISteamFriends_GetFriendRelationship(struct ISteamFriends *iface, union CSteamID steam_id_friend)
{
	struct ISteamFriendsImpl *This = impl_from_ISteamFriends(iface);

	LOG_ENTER_NOTIMPL("(This = %p, steam_id_friend = %#" PRIx64 ")", VOIDPTR(This), steam_id_friend.raw);

	return STEAM_FRIENDS_FRIEND_RELATIONSHIP_FRIEND;
}

MEMBER enum steam_friends_persona_state ISteamFriends_GetFriendPersonaState(struct ISteamFriends *iface, union CSteamID steam_id_friend)
{
	struct ISteamFriendsImpl *This = impl_from_ISteamFriends(iface);

	LOG_ENTER_NOTIMPL("(This = %p, steam_id_friend = %#" PRIx64 ")", VOIDPTR(This), steam_id_friend.raw);

	return STEAM_FRIENDS_PERSONA_STATE_ONLINE;
}

MEMBER const char *ISteamFriends_GetFriendPersonaName(struct ISteamFriends *iface, union CSteamID steam_id_friend)
{
	struct ISteamFriendsImpl *This = impl_from_ISteamFriends(iface);

	LOG_ENTER_NOTIMPL("(This = %p, steam_id_friend = %#" PRIx64 ")", VOIDPTR(This), steam_id_friend.raw);

	return "";
}

MEMBER steam_bool_t ISteamFriends_GetFriendGamePlayed005(struct ISteamFriends *iface, union CSteamID steam_id_friend, struct steam_friends_friend_game_info *info)
{
	struct ISteamFriendsImpl *This = impl_from_ISteamFriends(iface);

	LOG_ENTER_NOTIMPL("(This = %p, steam_id_friend = %#" PRIx64 ", info = %p)", VOIDPTR(This), steam_id_friend.raw, VOIDPTR(info));

	return STEAM_FALSE;
}

MEMBER const char *ISteamFriends_GetPlayerNickname(struct ISteamFriends *iface, union CSteamID steam_id_player)
{
	struct ISteamFriendsImpl *This = impl_from_ISteamFriends(iface);

	LOG_ENTER_NOTIMPL("(This = %p, steam_id_player = %#" PRIx64 ")", VOIDPTR(This), steam_id_player.raw);

	return "";
}

EXPORT const char *SteamAPI_ISteamFriends_GetPlayerNickname(struct ISteamFriends *iface, union CSteamID steam_id_player)
{
	return ISteamFriends_GetPlayerNickname(iface, steam_id_player);
}

MEMBER int ISteamFriends_GetSmallFriendAvatar(struct ISteamFriends *iface, union CSteamID steam_id_friend)
{
	struct ISteamFriendsImpl *This = impl_from_ISteamFriends(iface);

	LOG_ENTER_NOTIMPL("(This = %p, steam_id_friend = %#" PRIx64 ")", VOIDPTR(This), steam_id_friend.raw);

	return 0;
}

MEMBER int ISteamFriends_GetMediumFriendAvatar(struct ISteamFriends *iface, union CSteamID steam_id_friend)
{
	struct ISteamFriendsImpl *This = impl_from_ISteamFriends(iface);

	LOG_ENTER_NOTIMPL("(This = %p, steam_id_friend = %#" PRIx64 ")", VOIDPTR(This), steam_id_friend.raw);

	return 0;
}

MEMBER int ISteamFriends_GetLargeFriendAvatar(struct ISteamFriends *iface, union CSteamID steam_id_friend)
{
	struct ISteamFriendsImpl *This = impl_from_ISteamFriends(iface);

	LOG_ENTER_NOTIMPL("(This = %p, steam_id_friend = %#" PRIx64 ")", VOIDPTR(This), steam_id_friend.raw);

	return 0;
}

MEMBER steam_bool_t ISteamFriends_SetRichPresence(struct ISteamFriends *iface, const char *key, const char *value)
{
	struct ISteamFriendsImpl *This = impl_from_ISteamFriends(iface);

	LOG_ENTER_NOTIMPL("(This = %p, key = \"%s\", value = \"%s\")", VOIDPTR(This), debug_str(key), debug_str(value));

	return STEAM_TRUE;
}

MEMBER void ISteamFriends_ClearRichPresence(struct ISteamFriends *iface)
{
	struct ISteamFriendsImpl *This = impl_from_ISteamFriends(iface);

	LOG_ENTER_NOTIMPL("(This = %p)", VOIDPTR(This));
}

MEMBER int ISteamFriends_GetFriendRichPresenceKeyCount(struct ISteamFriends *iface, union CSteamID steam_id_friend)
{
	struct ISteamFriendsImpl *This = impl_from_ISteamFriends(iface);

	LOG_ENTER_NOTIMPL("(This = %p, steam_id_friend = %#" PRIx64 ")", VOIDPTR(This), steam_id_friend.raw);

	return 0;
}

MEMBER steam_bool_t ISteamFriends_InviteUserToGame(struct ISteamFriends *iface, union CSteamID steam_id_friend, const char *connect_str)
{
	struct ISteamFriendsImpl *This = impl_from_ISteamFriends(iface);

	LOG_ENTER_NOTIMPL("(This = %p, steam_id_friend = %#" PRIx64 ", connect_str = \"%s\")", VOIDPTR(This), steam_id_friend.raw, debug_str(connect_str));

	return STEAM_FALSE;
}

struct ISteamFriends *SteamFriends_generic(const char *version)
{
	static const struct
	{
		const char *name;
		struct ISteamFriends *(*iface_getter)(void);
	} ifaces[] = {
		{ STEAMFRIENDS_INTERFACE_VERSION_001, SteamFriends001 },
		{ STEAMFRIENDS_INTERFACE_VERSION_013, SteamFriends013 },
		{ STEAMFRIENDS_INTERFACE_VERSION_014, SteamFriends014 },
		{ STEAMFRIENDS_INTERFACE_VERSION_015, SteamFriends015 },
		{ STEAMFRIENDS_INTERFACE_VERSION_017, SteamFriends017 },
		{ NULL, NULL }
	};
	int i;

	LOG_ENTER("(version = \"%s\")", debug_str(version));

	i = 0;
	while (ifaces[i].name)
	{
		if (strcmp(ifaces[i].name, version) == 0)
		{
			if (ifaces[i].iface_getter)
				return ifaces[i].iface_getter();

			break;
		}
		i++;
	}

	WARN("Unable to find ISteamFriends version \"%s\".", debug_str(version));

	return NULL;
}

void SteamFriends_set_version(const char *version)
{
	LOG_ENTER("(version = \"%s\")", debug_str(version));

	if (!steam_friends_version)
		steam_friends_version = version;
}

EXPORT struct ISteamFriends *SteamFriends(void)
{
	static struct ISteamFriends *cached_iface = NULL;

	LOG_ENTER0("()");

	if (!steam_friends_version)
	{
		steam_friends_version = STEAMFRIENDS_INTERFACE_VERSION_015;

		WARN("ISteamFriends: No version specified, defaulting to \"%s\".", debug_str(steam_friends_version));
	}

	if (!cached_iface)
		cached_iface = SteamFriends_generic(steam_friends_version);

	return cached_iface;
}
