#include "debug.h"

#include "ISteamFriends001.h"
#include "ISteamFriends_priv.h"

static const struct ISteamFriends001Vtbl ISteamFriends001_vtbl = {
	ISteamFriends_GetPersonaName,
	DBG_TRAP,
	ISteamFriends_GetPersonaState,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	ISteamFriends_GetFriendRelationship,
	ISteamFriends_GetFriendPersonaState,
	DBG_TRAP,
	ISteamFriends_GetFriendPersonaName,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP
};

struct ISteamFriends *SteamFriends001(void)
{
	static struct ISteamFriendsImpl impl;

	impl.base.vtbl.v001 = &ISteamFriends001_vtbl;

	return &impl.base;
}
