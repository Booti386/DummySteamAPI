#ifndef ISTEAMFRIENDS_H
#define ISTEAMFRIENDS_H 1

#define STEAMFRIENDS_INTERFACE_VERSION_PREFIX "SteamFriends"

struct ISteamFriends
{
	union
	{
		const void *ptr;
		const struct ISteamFriends001Vtbl *v001;
		const struct ISteamFriends013Vtbl *v013;
		const struct ISteamFriends014Vtbl *v014;
		const struct ISteamFriends015Vtbl *v015;
		const struct ISteamFriends017Vtbl *v017;
	} vtbl;
};

enum steam_friends_persona_state
{
	STEAM_FRIENDS_PERSONA_STATE_OFFLINE = 0u,
	STEAM_FRIENDS_PERSONA_STATE_ONLINE = 1u,
	STEAM_FRIENDS_PERSONA_STATE_BUSY = 2u,
	STEAM_FRIENDS_PERSONA_STATE_AWAY = 3u,
	STEAM_FRIENDS_PERSONA_STATE_SNOOZE = 4u,
	STEAM_FRIENDS_PERSONA_STATE_LOOKING_TO_TRADE = 5u,
	STEAM_FRIENDS_PERSONA_STATE_LOOKING_TO_PLAY = 6u
};

enum steam_friends_friend_relationship
{
	STEAM_FRIENDS_FRIEND_RELATIONSHIP_NONE = 0u,
	STEAM_FRIENDS_FRIEND_RELATIONSHIP_BLOCKED = 1u,
	STEAM_FRIENDS_FRIEND_RELATIONSHIP_REQUEST_RECIPIENT = 2u,
	STEAM_FRIENDS_FRIEND_RELATIONSHIP_FRIEND = 3u,
	STEAM_FRIENDS_FRIEND_RELATIONSHIP_REQUEST_INITIATOR = 4u,
	STEAM_FRIENDS_FRIEND_RELATIONSHIP_IGNORED = 5u,
	STEAM_FRIENDS_FRIEND_RELATIONSHIP_IGNORED_FRIEND = 6u,
	STEAM_FRIENDS_FRIEND_RELATIONSHIP_SUGGESTED = 7u
};

PACKED_STRUCT steam_friends_friend_game_info
{
	PACKED_STRUCT_MEMBER(union CGameID, game_id);
	PACKED_STRUCT_MEMBER(uint32_t, game_ip);
	PACKED_STRUCT_MEMBER(uint16_t, game_port);
	PACKED_STRUCT_MEMBER(uint16_t, query_port);
	PACKED_STRUCT_MEMBER(union CSteamID, steam_id_lobby);
};

PACKED_STRUCT steam_callback_data_friends_game_overlay_activated
{
	PACKED_STRUCT_MEMBER(uint8_t, active);
};

struct ISteamFriends *SteamFriends_generic(const char *version);
void SteamFriends_set_version(const char *version);
extern struct ISteamFriends *SteamFriends(void);

#endif /* ISTEAMFRIENDS_H */
