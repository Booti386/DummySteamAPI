#ifndef ISTEAMFRIENDS014_H
#define ISTEAMFRIENDS014_H 1

#include "steam.h"

#include "ISteamFriends.h"

#define STEAMFRIENDS_INTERFACE_VERSION_014 "SteamFriends014"

struct ISteamFriends014Vtbl
{
	MEMBER const char *(*GetPersonaName)(struct ISteamFriends *iface);
	void *SetPersonaName;
	MEMBER enum steam_friends_persona_state (*GetPersonaState)(struct ISteamFriends *iface);
	MEMBER int (*GetFriendCount)(struct ISteamFriends *iface, int friend_flags);
	void *GetFriendByIndex;
	MEMBER enum steam_friends_friend_relationship (*GetFriendRelationship)(struct ISteamFriends *iface, union CSteamID steam_id_friend);
	MEMBER enum steam_friends_persona_state (*GetFriendPersonaState)(struct ISteamFriends *iface, union CSteamID steam_id_friend);
	MEMBER const char *(*GetFriendPersonaName)(struct ISteamFriends *iface, union CSteamID steam_id_friend);
	MEMBER steam_bool_t (*GetFriendGamePlayed005)(struct ISteamFriends *iface, union CSteamID steam_id_friend, struct steam_friends_friend_game_info *info);
	void *GetFriendPersonaNameHistory;
	MEMBER const char *(*GetPlayerNickname)(struct ISteamFriends *iface, union CSteamID steam_id_player);
	void *HasFriend;
	void *GetClanCount;
	void *GetClanByIndex;
	void *GetClanName;
	void *GetClanTag;
	void *GetClanActivityCounts;
	void *DownloadClanActivityCounts;
	void *GetFriendCountFromSource;
	void *GetFriendFromSourceByIndex;
	void *IsUserInSource;
	void *SetInGameVoiceSpeaking;
	void *ActivateGameOverlay;
	void *ActivateGameOverlayToUser;
	void *ActivateGameOverlayToWebPage;
	void *ActivateGameOverlayToStore;
	void *SetPlayedWith;
	void *ActivateGameOverlayInviteDialog;
	MEMBER int (*GetSmallFriendAvatar)(struct ISteamFriends *iface, union CSteamID steam_id_friend);
	MEMBER int (*GetMediumFriendAvatar)(struct ISteamFriends *iface, union CSteamID steam_id_friend);
	MEMBER int (*GetLargeFriendAvatar)(struct ISteamFriends *iface, union CSteamID steam_id_friend);
	void *RequestUserInformation;
	void *RequestClanOfficerList;
	void *GetClanOwner;
	void *GetClanOfficerCount;
	void *GetClanOfficerByIndex;
	void *GetUserRestrictions;
	MEMBER steam_bool_t (*SetRichPresence)(struct ISteamFriends *iface, const char *key, const char *value);
	MEMBER void (*ClearRichPresence)(struct ISteamFriends *iface);
	void *GetFriendRichPresence;
	MEMBER int (*GetFriendRichPresenceKeyCount)(struct ISteamFriends *iface, union CSteamID steam_id_friend);
	void *GetFriendRichPresenceKeyByIndex;
	void *RequestFriendRichPresence;
	MEMBER steam_bool_t (*InviteUserToGame)(struct ISteamFriends *iface, union CSteamID steam_id_friend, const char *connect_str);
	void *GetCoplayFriendCount;
	void *GetCoplayFriend;
	void *GetFriendCoplayTime;
	void *GetFriendCoplayGame;
	void *JoinClanChatRoom;
	void *LeaveClanChatRoom;
	void *GetClanChatMemberCount;
	void *GetChatMemberByIndex;
	void *SendClanChatMessage;
	void *GetClanChatMessage;
	void *IsClanChatAdmin;
	void *IsClanChatWindowOpenInSteam;
	void *OpenClanChatWindowInSteam;
	void *CloseClanChatWindowInSteam;
	void *SetListenForFriendsMessages;
	void *ReplyToFriendMessage;
	void *GetFriendMessage;
	void *GetFollowerCount;
	void *IsFollowing;
	void *EnumerateFollowingList;
};

struct ISteamFriends *SteamFriends014(void);

#endif /* ISTEAMFRIENDS014_H */
