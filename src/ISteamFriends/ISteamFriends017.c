#include "debug.h"

#include "ISteamFriends017.h"
#include "ISteamFriends_priv.h"

static const struct ISteamFriends017Vtbl ISteamFriends017_vtbl = {
	ISteamFriends_GetPersonaName,
	DBG_TRAP,
	ISteamFriends_GetPersonaState,
	ISteamFriends_GetFriendCount,
	DBG_TRAP,
	ISteamFriends_GetFriendRelationship,
	ISteamFriends_GetFriendPersonaState,
	ISteamFriends_GetFriendPersonaName,
	ISteamFriends_GetFriendGamePlayed005,
	DBG_TRAP,
	DBG_TRAP,
	ISteamFriends_GetPlayerNickname,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	ISteamFriends_GetSmallFriendAvatar,
	ISteamFriends_GetMediumFriendAvatar,
	ISteamFriends_GetLargeFriendAvatar,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	ISteamFriends_SetRichPresence,
	ISteamFriends_ClearRichPresence,
	DBG_TRAP,
	ISteamFriends_GetFriendRichPresenceKeyCount,
	DBG_TRAP,
	DBG_TRAP,
	ISteamFriends_InviteUserToGame,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP
};

struct ISteamFriends *SteamFriends017(void)
{
	static struct ISteamFriendsImpl impl;

	impl.base.vtbl.v017 = &ISteamFriends017_vtbl;

	return &impl.base;
}
