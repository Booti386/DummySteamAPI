#ifndef ISTEAMUSER_H
#define ISTEAMUSER_H 1

#include "steam.h"

#define STEAMUSER_INTERFACE_VERSION_PREFIX "SteamUser"

struct ISteamUser
{
	union
	{
		const void *ptr;
		const struct ISteamUser004Vtbl *v004;
		const struct ISteamUser016Vtbl *v016;
		const struct ISteamUser017Vtbl *v017;
		const struct ISteamUser018Vtbl *v018;
		const struct ISteamUser019Vtbl *v019;
		const struct ISteamUser020Vtbl *v020;
	} vtbl;
};

typedef uint32_t steam_auth_ticket_handle_t;

enum steam_user_has_license_for_app_result
{
	STEAM_USER_HAS_LICENSE_FOR_APP_RESULT_HAS_LICENSE = 0u,
	STEAM_USER_HAS_LICENSE_FOR_APP_RESULT_DOES_NOT_HAVE_LICENSE = 1u,
	STEAM_USER_HAS_LICENSE_FOR_APP_RESULT_NO_AUTH = 2u
};

PACKED_STRUCT steam_callback_data_user_encrypted_app_ticket_response
{
	PACKED_STRUCT_MEMBER(enum steam_result, result);
};

PACKED_STRUCT steam_callback_data_user_get_auth_session_ticket_response
{
	PACKED_STRUCT_MEMBER(steam_auth_ticket_handle_t, ticket);
	PACKED_STRUCT_MEMBER(enum steam_result, result);
};

struct ISteamUser *SteamUser_generic(const char *version);
void SteamUser_set_version(const char *version);
extern struct ISteamUser *SteamUser(void);

#endif /* ISTEAMUSER_H */
