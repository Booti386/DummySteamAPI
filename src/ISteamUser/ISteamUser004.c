#include "debug.h"

#include "ISteamUser004.h"
#include "ISteamUser_priv.h"

static const struct ISteamUser004Vtbl ISteamUser004_vtbl = {
	ISteamUser_GetHSteamUser,
	DBG_TRAP,
	DBG_TRAP,
	ISteamUser_BLoggedOn,
	DBG_TRAP,
	DBG_TRAP,
	ISteamUser_GetSteamID,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	ISteamUser_InitiateGameConnection,
	ISteamUser_TerminateGameConnection,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP
};

struct ISteamUser *SteamUser004(void)
{
	static struct ISteamUserImpl impl;

	impl.base.vtbl.v004 = &ISteamUser004_vtbl;

	return &impl.base;
}
