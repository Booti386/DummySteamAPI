#include "debug.h"

#include "ISteamUser018.h"
#include "ISteamUser_priv.h"

static const struct ISteamUser018Vtbl ISteamUser018_vtbl = {
	ISteamUser_GetHSteamUser,
	ISteamUser_BLoggedOn,
	ISteamUser_GetSteamID,
	ISteamUser_InitiateGameConnection010,
	ISteamUser_TerminateGameConnection,
	DBG_TRAP,
	ISteamUser_GetUserDataFolder,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	ISteamUser_GetAuthSessionTicket,
	DBG_TRAP,
	DBG_TRAP,
	ISteamUser_CancelAuthTicket,
	ISteamUser_UserHasLicenseForApp,
	ISteamUser_BIsBehindNAT,
	ISteamUser_AdvertiseGame,
	ISteamUser_RequestEncryptedAppTicket,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP
};

struct ISteamUser *SteamUser018(void)
{
	static struct ISteamUserImpl impl;

	impl.base.vtbl.v018 = &ISteamUser018_vtbl;

	return &impl.base;
}
