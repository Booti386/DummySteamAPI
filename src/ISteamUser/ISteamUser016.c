#include "debug.h"

#include "ISteamUser016.h"
#include "ISteamUser_priv.h"

static const struct ISteamUser016Vtbl ISteamUser016_vtbl = {
	ISteamUser_GetHSteamUser,
	ISteamUser_BLoggedOn,
	ISteamUser_GetSteamID,
	ISteamUser_InitiateGameConnection010,
	ISteamUser_TerminateGameConnection,
	DBG_TRAP,
	ISteamUser_GetUserDataFolder,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	ISteamUser_GetAuthSessionTicket,
	DBG_TRAP,
	DBG_TRAP,
	ISteamUser_CancelAuthTicket,
	ISteamUser_UserHasLicenseForApp,
	ISteamUser_BIsBehindNAT,
	ISteamUser_AdvertiseGame,
	ISteamUser_RequestEncryptedAppTicket,
	DBG_TRAP,
};

struct ISteamUser *SteamUser016(void)
{
	static struct ISteamUserImpl impl;

	impl.base.vtbl.v016 = &ISteamUser016_vtbl;

	return &impl.base;
}
