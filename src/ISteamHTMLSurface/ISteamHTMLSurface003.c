#include "debug.h"

#include "ISteamHTMLSurface003.h"
#include "ISteamHTMLSurface_priv.h"

static const struct ISteamHTMLSurface003Vtbl ISteamHTMLSurface003_vtbl = {
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP
};

struct ISteamHTMLSurface *SteamHTMLSurface003(void)
{
	static struct ISteamHTMLSurfaceImpl impl;

	impl.base.vtbl.v003 = &ISteamHTMLSurface003_vtbl;

	return &impl.base;
}
