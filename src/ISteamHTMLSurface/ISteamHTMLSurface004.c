#include "debug.h"

#include "ISteamHTMLSurface004.h"
#include "ISteamHTMLSurface_priv.h"

static const struct ISteamHTMLSurface004Vtbl ISteamHTMLSurface004_vtbl = {
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP
};

struct ISteamHTMLSurface *SteamHTMLSurface004(void)
{
	static struct ISteamHTMLSurfaceImpl impl;

	impl.base.vtbl.v004 = &ISteamHTMLSurface004_vtbl;

	return &impl.base;
}
