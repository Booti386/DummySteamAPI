#include "debug.h"

#include "ISteamRemoteStorage013.h"
#include "ISteamRemoteStorage_priv.h"

static const struct ISteamRemoteStorage013Vtbl ISteamRemoteStorage013_vtbl = {
	ISteamRemoteStorage_FileWrite,
	ISteamRemoteStorage_FileRead,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	ISteamRemoteStorage_FileWriteStreamOpen,
	ISteamRemoteStorage_FileWriteStreamWriteChunk,
	ISteamRemoteStorage_FileWriteStreamClose,
	ISteamRemoteStorage_FileWriteStreamCancel,
	ISteamRemoteStorage_FileExists,
	DBG_TRAP,
	ISteamRemoteStorage_GetFileSize,
	ISteamRemoteStorage_GetFileTimestamp,
	DBG_TRAP,
	ISteamRemoteStorage_GetFileCount,
	DBG_TRAP,
	ISteamRemoteStorage_GetQuota,
	ISteamRemoteStorage_IsCloudEnabledForAccount,
	ISteamRemoteStorage_IsCloudEnabledForApp,
	ISteamRemoteStorage_SetCloudEnabledForApp,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	ISteamRemoteStorage_EnumerateUserPublishedFiles,
	DBG_TRAP,
	ISteamRemoteStorage_EnumerateUserSubscribedFiles,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	ISteamRemoteStorage_EnumeratePublishedFilesByUserAction,
	DBG_TRAP,
	DBG_TRAP
};

struct ISteamRemoteStorage *SteamRemoteStorage013(void)
{
	static struct ISteamRemoteStorageImpl impl;

	impl.base.vtbl.v013 = &ISteamRemoteStorage013_vtbl;

	return &impl.base;
}
