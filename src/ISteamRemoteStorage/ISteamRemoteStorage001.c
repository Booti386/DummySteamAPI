#include "debug.h"

#include "ISteamRemoteStorage001.h"
#include "ISteamRemoteStorage_priv.h"

static const struct ISteamRemoteStorage001Vtbl ISteamRemoteStorage001_vtbl = {
	DBG_TRAP,
	ISteamRemoteStorage_GetFileSize,
	DBG_TRAP,
	ISteamRemoteStorage_FileExists,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	ISteamRemoteStorage_GetQuota
};

struct ISteamRemoteStorage *SteamRemoteStorage001(void)
{
	static struct ISteamRemoteStorageImpl impl;

	impl.base.vtbl.v001 = &ISteamRemoteStorage001_vtbl;

	return &impl.base;
}
