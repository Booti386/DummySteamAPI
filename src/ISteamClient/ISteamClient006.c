#include "debug.h"

#include "ISteamClient006.h"
#include "ISteamClient_priv.h"

static const struct ISteamClient006Vtbl ISteamClient006_vtbl = {
	ISteamClient_CreateSteamPipe,
	ISteamClient_BReleaseSteamPipe,
	DBG_TRAP,
	ISteamClient_ConnectToGlobalUser,
	DBG_TRAP,
	ISteamClient_ReleaseUser,
	ISteamClient_GetISteamUser,
	DBG_TRAP,
	ISteamClient_GetISteamGameServer,
	DBG_TRAP,
	DBG_TRAP,
	ISteamClient_GetISteamFriends,
	ISteamClient_GetISteamUtils,
	DBG_TRAP,
	ISteamClient_GetISteamMatchmaking,
	ISteamClient_GetISteamApps,
	DBG_TRAP,
	DBG_TRAP,
	ISteamClient_GetISteamMatchmakingServers,
	DBG_TRAP,
	DBG_TRAP
};

struct ISteamClient *SteamClient006(void)
{
	static struct ISteamClientImpl impl;

	impl.base.vtbl.v006 = &ISteamClient006_vtbl;

	return &impl.base;
}
