#include "debug.h"

#include "ISteamClient019.h"
#include "ISteamClient_priv.h"

static const struct ISteamClient019Vtbl ISteamClient019_vtbl = {
	ISteamClient_CreateSteamPipe,
	ISteamClient_BReleaseSteamPipe,
	ISteamClient_ConnectToGlobalUser,
	DBG_TRAP,
	ISteamClient_ReleaseUser,
	ISteamClient_GetISteamUser,
	ISteamClient_GetISteamGameServer,
	DBG_TRAP,
	ISteamClient_GetISteamFriends,
	ISteamClient_GetISteamUtils,
	ISteamClient_GetISteamMatchmaking,
	ISteamClient_GetISteamMatchmakingServers,
	ISteamClient_GetISteamGenericInterface,
	ISteamClient_GetISteamUserStats,
	ISteamClient_GetISteamGameServerStats,
	ISteamClient_GetISteamApps,
	ISteamClient_GetISteamNetworking,
	ISteamClient_GetISteamRemoteStorage,
	ISteamClient_GetISteamScreenshots,
	DBG_TRAP,
	DBG_TRAP,
	ISteamClient_SetWarningMessageHook,
	ISteamClient_BShutdownIfAllPipesClosed,
	ISteamClient_GetISteamHTTP,
	ISteamClient_GetISteamUnifiedMessages,
	ISteamClient_GetISteamController,
	ISteamClient_GetISteamUGC,
	ISteamClient_GetISteamAppList,
	ISteamClient_GetISteamMusic,
	ISteamClient_GetISteamMusicRemote,
	ISteamClient_GetISteamHTMLSurface,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	ISteamClient_GetISteamInventory,
	ISteamClient_GetISteamVideo,
	ISteamClient_GetISteamParentalSettings,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP
};

struct ISteamClient *SteamClient019(void)
{
	static struct ISteamClientImpl impl;

	impl.base.vtbl.v019 = &ISteamClient019_vtbl;

	return &impl.base;
}
