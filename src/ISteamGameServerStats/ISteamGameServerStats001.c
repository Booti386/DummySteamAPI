#include "debug.h"

#include "ISteamGameServerStats001.h"
#include "ISteamGameServerStats_priv.h"

static const struct ISteamGameServerStats001Vtbl ISteamGameServerStats001_vtbl = {
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP
};

struct ISteamGameServerStats *SteamGameServerStats001(void)
{
	static struct ISteamGameServerStatsImpl impl;

	impl.base.vtbl.v001 = &ISteamGameServerStats001_vtbl;

	return &impl.base;
}
