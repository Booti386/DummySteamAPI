#include "debug.h"

#include "ISteamApps001.h"
#include "ISteamApps_priv.h"

static const struct ISteamApps001Vtbl ISteamApps001_vtbl = {
	DBG_TRAP
};

struct ISteamApps *SteamApps001(void)
{
	static struct ISteamAppsImpl impl;

	impl.base.vtbl.v001 = &ISteamApps001_vtbl;

	return &impl.base;
}
