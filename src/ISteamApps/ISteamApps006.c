#include "debug.h"

#include "ISteamApps006.h"
#include "ISteamApps_priv.h"

static const struct ISteamApps006Vtbl ISteamApps006_vtbl = {
	ISteamApps_BIsSubscribed,
	ISteamApps_BIsLowViolence,
	ISteamApps_BIsCybercafe,
	ISteamApps_BIsVACBanned,
	ISteamApps_GetCurrentGameLanguage,
	ISteamApps_GetAvailableGameLanguages,
	ISteamApps_BIsSubscribedApp,
	ISteamApps_BIsDlcInstalled,
	DBG_TRAP,
	DBG_TRAP,
	ISteamApps_GetDLCCount,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	ISteamApps_GetCurrentBetaName,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	ISteamApps_GetLaunchQueryParam,
	DBG_TRAP
};

struct ISteamApps *SteamApps006(void)
{
	static struct ISteamAppsImpl impl;

	impl.base.vtbl.v006 = &ISteamApps006_vtbl;

	return &impl.base;
}
