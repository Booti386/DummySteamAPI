#include "debug.h"

#include "ISteamApps005.h"
#include "ISteamApps_priv.h"

static const struct ISteamApps005Vtbl ISteamApps005_vtbl = {
	ISteamApps_BIsSubscribed,
	ISteamApps_BIsLowViolence,
	ISteamApps_BIsCybercafe,
	ISteamApps_BIsVACBanned,
	ISteamApps_GetCurrentGameLanguage,
	ISteamApps_GetAvailableGameLanguages,
	ISteamApps_BIsSubscribedApp,
	ISteamApps_BIsDlcInstalled,
	DBG_TRAP,
	DBG_TRAP,
	ISteamApps_GetDLCCount,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	ISteamApps_GetCurrentBetaName,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP
};

struct ISteamApps *SteamApps005(void)
{
	static struct ISteamAppsImpl impl;

	impl.base.vtbl.v005 = &ISteamApps005_vtbl;

	return &impl.base;
}
