#include "debug.h"

#include "ISteamController005.h"
#include "ISteamController_priv.h"

static const struct ISteamController005Vtbl ISteamController005_vtbl = {
	ISteamController_Init003,
	ISteamController_Shutdown,
	ISteamController_RunFrame,
	ISteamController_GetConnectedControllers,
	DBG_TRAP,
	ISteamController_GetActionSetHandle,
	ISteamController_ActivateActionSet,
	DBG_TRAP,
	ISteamController_GetDigitalActionHandle,
	DBG_TRAP,
	DBG_TRAP,
	ISteamController_GetAnalogActionHandle,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP
};

struct ISteamController *SteamController005(void)
{
	static struct ISteamControllerImpl impl;

	impl.base.vtbl.v005 = &ISteamController005_vtbl;

	return &impl.base;
}
