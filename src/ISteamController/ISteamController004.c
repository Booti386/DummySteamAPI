#include "debug.h"

#include "ISteamController004.h"
#include "ISteamController_priv.h"

static const struct ISteamController004Vtbl ISteamController004_vtbl = {
	ISteamController_Init003,
	ISteamController_Shutdown,
	ISteamController_RunFrame,
	ISteamController_GetConnectedControllers,
	DBG_TRAP,
	ISteamController_GetActionSetHandle,
	ISteamController_ActivateActionSet,
	DBG_TRAP,
	ISteamController_GetDigitalActionHandle,
	DBG_TRAP,
	DBG_TRAP,
	ISteamController_GetAnalogActionHandle,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP
};

struct ISteamController *SteamController004(void)
{
	static struct ISteamControllerImpl impl;

	impl.base.vtbl.v004 = &ISteamController004_vtbl;

	return &impl.base;
}
