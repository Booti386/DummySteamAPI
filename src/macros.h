#ifndef MACROS_H
#define MACROS_H 1

#include <stddef.h> /* offsetof() */
#include <stdint.h>
#include <stdlib.h>

/* Force warning for enum value not handled in switch (even with default:). */
#pragma GCC diagnostic warning "-Wswitch-enum"

#if CONFIG_OS_WINDOWS

# define PACKED_STRUCT_ALIGNMENT 1
# define PACKED_STRUCT_MEMBER_MAX_ALIGNMENT 8

# define EXPORT __declspec(dllexport)

# define DECL_FUNC_WITH_MEMBER_CALLBACK_PARAM __extension__
# define MEMBER_CALLBACK_PARAM __attribute__((thiscall)) __attribute__((ms_abi))
# define MEMBER DECL_FUNC_WITH_MEMBER_CALLBACK_PARAM MEMBER_CALLBACK_PARAM
# define DSA_MEMBER_RETURN_STRUCT0(ret_type, ret_param, name, param_this) MEMBER ret_type *name(param_this, ret_type *ret_param)
# define DSA_MEMBER_RETURN_STRUCT(ret_type, ret_param, name, param_this, ...) MEMBER ret_type *name(param_this, ret_type *ret_param, __VA_ARGS__)
# define DSA_CALL_MEMBER_RETURN_STRUCT0(pfn, ret_ptr, param_this) ((void)(pfn)((param_this), (ret_ptr)))
# define DSA_CALL_MEMBER_RETURN_STRUCT(pfn, ret_ptr, param_this, ...) ((void)(pfn)((param_this), (ret_ptr), __VA_ARGS__))
# define DSA_MEMBER_RETURN_STRUCT_RETURN(ret_param, value) do { *(ret_param) = (value); return (ret_param); } while (0)

#else

# define PACKED_STRUCT_ALIGNMENT 1
# define PACKED_STRUCT_MEMBER_MAX_ALIGNMENT 4

# define EXPORT __attribute__((visibility("default")))

# define DECL_FUNC_WITH_MEMBER_CALLBACK_PARAM
# define MEMBER_CALLBACK_PARAM
# define MEMBER
# define DSA_MEMBER_RETURN_STRUCT0(ret_type, ret_param, name, param_this) MEMBER ret_type name(param_this)
# define DSA_MEMBER_RETURN_STRUCT(ret_type, ret_param, name, param_this, ...) MEMBER ret_type name(param_this, __VA_ARGS__)
# define DSA_CALL_MEMBER_RETURN_STRUCT0(pfn, ret_ptr, param_this) ((void)(*(ret_ptr) = (pfn)((param_this))))
# define DSA_CALL_MEMBER_RETURN_STRUCT(pfn, ret_ptr, param_this, ...) ((void)(*(ret_ptr) = (pfn)((param_this), __VA_ARGS__)))
# define DSA_MEMBER_RETURN_STRUCT_RETURN(ret_param, value) do { return (value); } while(0)

#endif

#define PACKED_STRUCT struct __attribute__((packed, aligned(PACKED_STRUCT_ALIGNMENT)))
#define PACKED_STRUCT_MEMBER(type, name) type name __attribute__((aligned(MIN2(sizeof(type), PACKED_STRUCT_MEMBER_MAX_ALIGNMENT))))
#define PACKED_STRUCT_MEMBER_ARRAY(type, name, size) type name[size] __attribute__((aligned(MIN2(sizeof(type[size]), PACKED_STRUCT_MEMBER_MAX_ALIGNMENT))))

#define DSA_PFN_MEMBER_RETURN_STRUCT0(ret_type, ret_param, name, param_this) DSA_MEMBER_RETURN_STRUCT0(ret_type, ret_param, (*name), param_this)
#define DSA_PFN_MEMBER_RETURN_STRUCT(ret_type, ret_param, name, param_this, ...) DSA_MEMBER_RETURN_STRUCT(ret_type, ret_param, (*name), param_this, __VA_ARGS__)

#define MAX2(x, y) ((x) > (y) ? (x) : (y))
#define MIN2(x, y) ((x) < (y) ? (x) : (y))

#define VOIDPTR(x) ((void *)(x))
#define ARRAY_SIZE(arr) (sizeof(arr) / sizeof((arr)[0]))
#define CONTAINER_OF(ptr, type, member) ((type *)((uintptr_t)(ptr) - offsetof(type, member)))
#define TO_STR_(x) #x
#define TO_STR(x) TO_STR_(x)

typedef void (*PFN_VOID)(void);

static inline void static_assert_ptr_eq_pfn(void) {
	_Static_assert(sizeof(void *) == sizeof(PFN_VOID), "Unsupported platform: Object and function pointer size mismatch");
}

static inline PFN_VOID voidptr_to_pfn(void *ptr) {
	static_assert_ptr_eq_pfn();
	return (PFN_VOID)(uintptr_t)ptr;
}

static inline void *pfn_to_voidptr(PFN_VOID pfn) {
	static_assert_ptr_eq_pfn();
	return VOIDPTR((uintptr_t)pfn);
}

#endif /* MACROS_H */
