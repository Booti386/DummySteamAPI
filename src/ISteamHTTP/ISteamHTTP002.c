#include "debug.h"

#include "ISteamHTTP002.h"
#include "ISteamHTTP_priv.h"

static const struct ISteamHTTP002Vtbl ISteamHTTP002_vtbl = {
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP
};

struct ISteamHTTP *SteamHTTP002(void)
{
	static struct ISteamHTTPImpl impl;

	impl.base.vtbl.v002 = &ISteamHTTP002_vtbl;

	return &impl.base;
}
