#include "debug.h"

#include "ISteamHTTP001.h"
#include "ISteamHTTP_priv.h"

static const struct ISteamHTTP001Vtbl ISteamHTTP001_vtbl = {
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP
};

struct ISteamHTTP *SteamHTTP001(void)
{
	static struct ISteamHTTPImpl impl;

	impl.base.vtbl.v001 = &ISteamHTTP001_vtbl;

	return &impl.base;
}
