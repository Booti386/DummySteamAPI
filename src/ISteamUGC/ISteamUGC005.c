#include "debug.h"

#include "ISteamUGC005.h"
#include "ISteamUGC_priv.h"

static const struct ISteamUGC005Vtbl ISteamUGC005_vtbl = {
	DBG_TRAP,
	DBG_TRAP,
	ISteamUGC_CreateQueryUGCDetailsRequest,
	ISteamUGC_SendQueryUGCRequest,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	ISteamUGC_ReleaseQueryUGCRequest,
	DBG_TRAP,
	DBG_TRAP,
	ISteamUGC_SetReturnLongDescription,
	ISteamUGC_SetReturnMetadata,
	ISteamUGC_SetReturnChildren,
	ISteamUGC_SetReturnAdditionalPreviews,
	ISteamUGC_SetReturnTotalOnly,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	ISteamUGC_GetNumSubscribedItems,
	ISteamUGC_GetSubscribedItems,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP
};

struct ISteamUGC *SteamUGC005(void)
{
	static struct ISteamUGCImpl impl;

	impl.base.vtbl.v005 = &ISteamUGC005_vtbl;

	return &impl.base;
}
