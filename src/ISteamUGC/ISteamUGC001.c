#include "debug.h"

#include "ISteamUGC001.h"
#include "ISteamUGC_priv.h"

static const struct ISteamUGC001Vtbl ISteamUGC001_vtbl = {
	DBG_TRAP,
	DBG_TRAP,
	ISteamUGC_SendQueryUGCRequest,
	DBG_TRAP,
	ISteamUGC_ReleaseQueryUGCRequest,
	DBG_TRAP,
	DBG_TRAP,
	ISteamUGC_SetReturnLongDescription,
	ISteamUGC_SetReturnTotalOnly,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP
};

struct ISteamUGC *SteamUGC001(void)
{
	static struct ISteamUGCImpl impl;

	impl.base.vtbl.v001 = &ISteamUGC001_vtbl;

	return &impl.base;
}
