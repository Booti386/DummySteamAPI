#include "debug.h"

#include "ISteamUGC009.h"
#include "ISteamUGC_priv.h"

static const struct ISteamUGC009Vtbl ISteamUGC009_vtbl = {
	DBG_TRAP,
	DBG_TRAP,
	ISteamUGC_CreateQueryUGCDetailsRequest,
	ISteamUGC_SendQueryUGCRequest,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	ISteamUGC_ReleaseQueryUGCRequest,
	DBG_TRAP,
	DBG_TRAP,
	ISteamUGC_SetReturnOnlyIDs,
	ISteamUGC_SetReturnKeyValueTags,
	ISteamUGC_SetReturnLongDescription,
	ISteamUGC_SetReturnMetadata,
	ISteamUGC_SetReturnChildren,
	ISteamUGC_SetReturnAdditionalPreviews,
	ISteamUGC_SetReturnTotalOnly,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	ISteamUGC_GetNumSubscribedItems,
	ISteamUGC_GetSubscribedItems,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP
};

struct ISteamUGC *SteamUGC009(void)
{
	static struct ISteamUGCImpl impl;

	impl.base.vtbl.v009 = &ISteamUGC009_vtbl;

	return &impl.base;
}
