#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>

#include "debug.h"
#include "steam.h"
#include "macros.h"

enum dsa_cfg
{
	/* DSA_CFG_LOG_LEVEL should be the first to avoid printing unwanted log messages. */
	DSA_CFG_LOG_LEVEL = 0,
	DSA_CFG_DATA_DIR,
	DSA_CFG_ORIG_STEAM_API_LIB,
	DSA_CFG_STEAM_APP_ID,
	DSA_CFG_STEAM_GAME_ID,
	DSA_CFG_STEAM_USER_ID
};

enum dsa_cfg_var_type
{
	DSA_CFG_VAR_TYPE_STR = 0,
	DSA_CFG_VAR_TYPE_U64
};

union dsa_cfg_var_val
{
	const char *str;
	uint64_t u64;
};

struct dsa_cfg_var
{
	const char *name;
	enum dsa_cfg_var_type type;
	union dsa_cfg_var_val val;
	unsigned int is_defined : 1;
};

#define DSA_CFG_VAR_STR(name, default_val) { name, DSA_CFG_VAR_TYPE_STR, { .str = default_val }, 0 }
#define DSA_CFG_VAR_U64(name, default_val) { name, DSA_CFG_VAR_TYPE_U64, { .str = default_val }, 0 }

static struct dsa_cfg_var cfg_vars[] = {
	[DSA_CFG_LOG_LEVEL]          = DSA_CFG_VAR_U64("DSA_LOG_LEVEL", "1"),
	[DSA_CFG_DATA_DIR]           = DSA_CFG_VAR_STR("DSA_DATA_DIR", ""),
	[DSA_CFG_ORIG_STEAM_API_LIB] = DSA_CFG_VAR_STR("DSA_ORIG_STEAM_API_LIB", ""),
	[DSA_CFG_STEAM_APP_ID]       = DSA_CFG_VAR_U64("SteamAppId", "1"),
	[DSA_CFG_STEAM_GAME_ID]      = DSA_CFG_VAR_U64("SteamGameId", "1"),
	[DSA_CFG_STEAM_USER_ID]      = DSA_CFG_VAR_U64("STEAM_USER_ID", "1")
};

#undef DSA_CFG_VAR_STR
#undef DSA_CFG_VAR_U64

int dsa_config_init(void)
{
	for (size_t i = 0; i < ARRAY_SIZE(cfg_vars); i++)
	{
		struct dsa_cfg_var *cfg_var = &cfg_vars[i];
		const char *data;

		data = getenv(cfg_var->name);
		if (!data)
		{
			data = cfg_var->val.str;
			WARN("Config var %s not defined. Defaulting to %s=\"%s\".", cfg_var->name, cfg_var->name, data);
		}

		switch (cfg_var->type)
		{
			case DSA_CFG_VAR_TYPE_STR:
				cfg_var->val.str = data;
				break;

			case DSA_CFG_VAR_TYPE_U64:
				cfg_var->val.u64 = strtoull(data, NULL, 0);
				break;
		}

		cfg_var->is_defined = 1;

		DEBUG("Config var %s=\"%s\".", cfg_var->name, data);
	}

	return 0;
}

enum dsa_log_level dsa_config_get_log_level(void)
{
	return (enum dsa_log_level)cfg_vars[DSA_CFG_LOG_LEVEL].val.u64;
}

const char *dsa_config_get_data_dir(void)
{
	return cfg_vars[DSA_CFG_DATA_DIR].val.str;
}

const char *dsa_config_get_orig_steam_api_lib(void)
{
	return cfg_vars[DSA_CFG_ORIG_STEAM_API_LIB].val.str;
}

steam_app_id_t dsa_config_get_steam_app_id(void)
{
	return (steam_app_id_t)cfg_vars[DSA_CFG_STEAM_APP_ID].val.u64;
}

union CGameID dsa_config_get_steam_game_id(void)
{
	union CGameID game_id;

	game_id.raw = cfg_vars[DSA_CFG_STEAM_GAME_ID].val.u64;
	return game_id;
}

steam_user_t dsa_config_get_steam_user_id(void)
{
	return (steam_user_t)cfg_vars[DSA_CFG_STEAM_USER_ID].val.u64;
}
