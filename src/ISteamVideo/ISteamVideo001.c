#include "debug.h"

#include "ISteamVideo001.h"
#include "ISteamVideo_priv.h"

static const struct ISteamVideo001Vtbl ISteamVideo001_vtbl = {
	DBG_TRAP,
	DBG_TRAP
};

struct ISteamVideo *SteamVideo001(void)
{
	static struct ISteamVideoImpl impl;

	impl.base.vtbl.v001 = &ISteamVideo001_vtbl;

	return &impl.base;
}
