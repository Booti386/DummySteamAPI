#ifndef ISTEAMUSERSTATS_H
#define ISTEAMUSERSTATS_H 1

#include "steam.h"

#define STEAMUSERSTATS_INTERFACE_VERSION_PREFIX "STEAMUSERSTATS_INTERFACE_VERSION"

struct ISteamUserStats
{
	union
	{
		const void *ptr;
		const struct ISteamUserStats011Vtbl *v011;
	} vtbl;
};

typedef uint64_t steam_leaderboard_t;

enum steam_user_stats_limit
{
	STEAM_USER_STATS_LIMIT_STAT_NAME_MAX = 128,
	STEAM_USER_STATS_LIMIT_LEADERBOARD_NAME_MAX = 128,
	STEAM_USER_STATS_LIMIT_LEADERBOARD_DETAILS_MAX = 64
};

enum steam_user_stats_leaderboard_display_type
{
	STEAM_USER_STATS_LEADERBOARD_DISPLAY_TYPE_NONE = 0u,
	STEAM_USER_STATS_LEADERBOARD_DISPLAY_TYPE_NUMERIC = 1u,
	STEAM_USER_STATS_LEADERBOARD_DISPLAY_TYPE_TIME_SEC = 2u,
	STEAM_USER_STATS_LEADERBOARD_DISPLAY_TYPE_TIME_MSEC = 3u
};

enum steam_user_stats_leaderboard_sort_method
{
	STEAM_USER_STATS_LEADERBOARD_SORT_METHOD_NONE = 0u,
	STEAM_USER_STATS_LEADERBOARD_SORT_METHOD_ASCENDING = 1u,
	STEAM_USER_STATS_LEADERBOARD_SORT_METHOD_DESCENDING = 2u
};

enum steam_user_stats_leaderboard_upload_score_method
{
	STEAM_USER_STATS_LEADERBOARD_UPLOAD_SCORE_METHOD_NONE = 0u,
	STEAM_USER_STATS_LEADERBOARD_UPLOAD_SCORE_METHOD_KEEP_BEST = 1u,
	STEAM_USER_STATS_LEADERBOARD_UPLOAD_SCORE_METHOD_FORCE_UPDATE = 2u
};

PACKED_STRUCT steam_callback_data_user_stats_user_stats_received
{
	PACKED_STRUCT_MEMBER(union CGameID, game_id);
	PACKED_STRUCT_MEMBER(enum steam_result, result);
	PACKED_STRUCT_MEMBER(union CSteamID, steam_id_user);
};

PACKED_STRUCT steam_callback_data_user_stats_user_stats_stored
{
	PACKED_STRUCT_MEMBER(union CGameID, game_id);
	PACKED_STRUCT_MEMBER(enum steam_result, result);
};

PACKED_STRUCT steam_callback_data_user_stats_user_achievement_stored
{
	PACKED_STRUCT_MEMBER(union CGameID, game_id);
	PACKED_STRUCT_MEMBER(steam_bool_t, is_group_achievement);
	PACKED_STRUCT_MEMBER_ARRAY(char, achievement_name, STEAM_USER_STATS_LIMIT_STAT_NAME_MAX);
	PACKED_STRUCT_MEMBER(uint32_t, cur_progress);
	PACKED_STRUCT_MEMBER(uint32_t, max_progress);
};

PACKED_STRUCT steam_callback_data_user_stats_leaderboard_find_result
{
	PACKED_STRUCT_MEMBER(steam_leaderboard_t, leaderboard);
	PACKED_STRUCT_MEMBER(steam_bool_t, found);
};

PACKED_STRUCT steam_callback_data_user_stats_leaderboard_score_uploaded
{
	PACKED_STRUCT_MEMBER(uint8_t, success);
	PACKED_STRUCT_MEMBER(steam_leaderboard_t, leaderboard);
	PACKED_STRUCT_MEMBER(int32_t, score);
	PACKED_STRUCT_MEMBER(uint8_t, score_changed);
	PACKED_STRUCT_MEMBER(int, global_rank);
	PACKED_STRUCT_MEMBER(int, prev_global_rank);
};

PACKED_STRUCT steam_callback_data_user_stats_number_of_current_players
{
	PACKED_STRUCT_MEMBER(uint8_t, success);
	PACKED_STRUCT_MEMBER(int32_t, player_count);
};

PACKED_STRUCT steam_callback_data_user_stats_global_stats_received
{
	PACKED_STRUCT_MEMBER(union CGameID, game_id);
	PACKED_STRUCT_MEMBER(enum steam_result, result);
};

struct ISteamUserStats *SteamUserStats_generic(const char *version);
void SteamUserStats_set_version(const char *version);
extern struct ISteamUserStats *SteamUserStats(void);

#endif /* ISTEAMUSERSTATS_H */
