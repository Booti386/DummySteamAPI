#include "debug.h"

#include "ISteamUtils006.h"
#include "ISteamUtils_priv.h"

static const struct ISteamUtils006Vtbl ISteamUtils006_vtbl = {
	ISteamUtils_GetSecondsSinceAppActive,
	DBG_TRAP,
	ISteamUtils_GetConnectedUniverse,
	ISteamUtils_GetServerRealTime,
	ISteamUtils_GetIPCountry,
	ISteamUtils_GetImageSize,
	ISteamUtils_GetImageRGBA,
	ISteamUtils_GetCSERIPPort,
	DBG_TRAP,
	ISteamUtils_GetAppID,
	ISteamUtils_SetOverlayNotificationPosition,
	ISteamUtils_IsAPICallCompleted,
	DBG_TRAP,
	ISteamUtils_GetAPICallResult,
	DBG_TRAP,
	DBG_TRAP,
	ISteamUtils_SetWarningMessageHook,
	ISteamUtils_IsOverlayEnabled,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	ISteamUtils_GetSteamUILanguage,
	DBG_TRAP
};

struct ISteamUtils *SteamUtils006(void)
{
	static struct ISteamUtilsImpl impl;

	impl.base.vtbl.v006 = &ISteamUtils006_vtbl;

	return &impl.base;
}
