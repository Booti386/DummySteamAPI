#include "debug.h"

#include "ISteamUtils007.h"
#include "ISteamUtils_priv.h"

static const struct ISteamUtils007Vtbl ISteamUtils007_vtbl = {
	ISteamUtils_GetSecondsSinceAppActive,
	DBG_TRAP,
	ISteamUtils_GetConnectedUniverse,
	ISteamUtils_GetServerRealTime,
	ISteamUtils_GetIPCountry,
	ISteamUtils_GetImageSize,
	ISteamUtils_GetImageRGBA,
	ISteamUtils_GetCSERIPPort,
	DBG_TRAP,
	ISteamUtils_GetAppID,
	ISteamUtils_SetOverlayNotificationPosition,
	ISteamUtils_IsAPICallCompleted,
	DBG_TRAP,
	ISteamUtils_GetAPICallResult,
	DBG_TRAP,
	DBG_TRAP,
	ISteamUtils_SetWarningMessageHook,
	ISteamUtils_IsOverlayEnabled,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	ISteamUtils_GetSteamUILanguage,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP
};

struct ISteamUtils *SteamUtils007(void)
{
	static struct ISteamUtilsImpl impl;

	impl.base.vtbl.v007 = &ISteamUtils007_vtbl;

	return &impl.base;
}
