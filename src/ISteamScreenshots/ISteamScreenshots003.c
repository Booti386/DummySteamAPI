#include "debug.h"

#include "ISteamScreenshots003.h"
#include "ISteamScreenshots_priv.h"

static const struct ISteamScreenshots003Vtbl ISteamScreenshots003_vtbl = {
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	ISteamScreenshots_HookScreenshots,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP
};

struct ISteamScreenshots *SteamScreenshots003(void)
{
	static struct ISteamScreenshotsImpl impl;

	impl.base.vtbl.v003 = &ISteamScreenshots003_vtbl;

	return &impl.base;
}
