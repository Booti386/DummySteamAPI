#include "debug.h"

#include "ISteamScreenshots002.h"
#include "ISteamScreenshots_priv.h"

static const struct ISteamScreenshots002Vtbl ISteamScreenshots002_vtbl = {
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	ISteamScreenshots_HookScreenshots,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP
};

struct ISteamScreenshots *SteamScreenshots002(void)
{
	static struct ISteamScreenshotsImpl impl;

	impl.base.vtbl.v002 = &ISteamScreenshots002_vtbl;

	return &impl.base;
}
