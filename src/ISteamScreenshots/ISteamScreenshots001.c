#include "debug.h"

#include "ISteamScreenshots001.h"
#include "ISteamScreenshots_priv.h"

static const struct ISteamScreenshots001Vtbl ISteamScreenshots001_vtbl = {
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	ISteamScreenshots_HookScreenshots,
	DBG_TRAP,
	DBG_TRAP
};

struct ISteamScreenshots *SteamScreenshots001(void)
{
	static struct ISteamScreenshotsImpl impl;

	impl.base.vtbl.v001 = &ISteamScreenshots001_vtbl;

	return &impl.base;
}
