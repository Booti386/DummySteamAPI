#include "debug.h"

#include "ISteamNetworking006.h"
#include "ISteamNetworking_priv.h"

static const struct ISteamNetworking006Vtbl ISteamNetworking006_vtbl = {
	DBG_TRAP,
	ISteamNetworking_IsP2PPacketAvailable,
	ISteamNetworking_ReadP2PPacket,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	ISteamNetworking_AllowP2PPacketRelay,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP
};

struct ISteamNetworking *SteamNetworking006(void)
{
	static struct ISteamNetworkingImpl impl;

	impl.base.vtbl.v006 = &ISteamNetworking006_vtbl;

	return &impl.base;
}
