#include "debug.h"

#include "ISteamNetworking005.h"
#include "ISteamNetworking_priv.h"

static const struct ISteamNetworking005Vtbl ISteamNetworking005_vtbl = {
	DBG_TRAP,
	ISteamNetworking_IsP2PPacketAvailable,
	ISteamNetworking_ReadP2PPacket,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	ISteamNetworking_AllowP2PPacketRelay,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP
};

struct ISteamNetworking *SteamNetworking005(void)
{
	static struct ISteamNetworkingImpl impl;

	impl.base.vtbl.v005 = &ISteamNetworking005_vtbl;

	return &impl.base;
}
