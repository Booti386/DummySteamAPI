#include "debug.h"

#include "ISteamGameServer011.h"
#include "ISteamGameServer_priv.h"

static const struct ISteamGameServer011Vtbl ISteamGameServer011_vtbl = {
	ISteamGameServer_InitGameServer,
	ISteamGameServer_SetProduct,
	ISteamGameServer_SetGameDescription,
	ISteamGameServer_SetModDir,
	ISteamGameServer_SetDedicatedServer,
	DBG_TRAP,
	ISteamGameServer_LogOnAnonymous,
	ISteamGameServer_LogOff,
	ISteamGameServer_BLoggedOn,
	ISteamGameServer_BSecure,
	ISteamGameServer_GetSteamID,
	DBG_TRAP,
	ISteamGameServer_SetMaxPlayerCount,
	ISteamGameServer_SetBotPlayerCount,
	ISteamGameServer_SetServerName,
	ISteamGameServer_SetMapName,
	ISteamGameServer_SetPasswordProtected,
	ISteamGameServer_SetSpectatorPort,
	DBG_TRAP,
	ISteamGameServer_ClearAllKeyValues,
	ISteamGameServer_SetKeyValue,
	ISteamGameServer_SetGameTags,
	DBG_TRAP,
	DBG_TRAP,
	ISteamGameServer_SendUserConnectAndAuthenticate,
	ISteamGameServer_CreateUnauthenticatedUserConnection,
	ISteamGameServer_SendUserDisconnect,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	ISteamGameServer_HandleIncomingPacket,
	DBG_TRAP,
	ISteamGameServer_EnableHeartbeats,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP
};

struct ISteamGameServer *SteamGameServer011(void)
{
	static struct ISteamGameServerImpl impl;

	impl.base.vtbl.v011 = &ISteamGameServer011_vtbl;

	return &impl.base;
}
