#include "debug.h"

#include "ISteamMatchmakingServers002.h"
#include "ISteamMatchmakingServers_priv.h"

static const struct ISteamMatchmakingServers002Vtbl ISteamMatchmakingServers002_vtbl = {
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	ISteamMatchmakingServers_CancelQuery,
	DBG_TRAP,
	ISteamMatchmakingServers_IsRefreshing,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP
};

struct ISteamMatchmakingServers *SteamMatchmakingServers002(void)
{
	static struct ISteamMatchmakingServersImpl impl;

	impl.base.vtbl.v002 = &ISteamMatchmakingServers002_vtbl;

	return &impl.base;
}
