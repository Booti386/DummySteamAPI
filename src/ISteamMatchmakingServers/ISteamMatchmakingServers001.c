#include "debug.h"

#include "ISteamMatchmakingServers001.h"
#include "ISteamMatchmakingServers_priv.h"

static const struct ISteamMatchmakingServers001Vtbl ISteamMatchmakingServers001_vtbl = {
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	ISteamMatchmakingServers_CancelQuery,
	DBG_TRAP,
	ISteamMatchmakingServers_IsRefreshing,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP,
	DBG_TRAP
};

struct ISteamMatchmakingServers *SteamMatchmakingServers001(void)
{
	static struct ISteamMatchmakingServersImpl impl;

	impl.base.vtbl.v001 = &ISteamMatchmakingServers001_vtbl;

	return &impl.base;
}
