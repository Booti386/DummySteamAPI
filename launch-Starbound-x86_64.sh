#!/bin/bash

set -e

DSA_DIR=$(realpath $(dirname "$0"))

SB_DIR="${HOME}/.steam/steam/steamapps/common/Starbound"
SB_BIN64="${SB_DIR}/linux/run-client.sh"
SB_LIB64="${SB_DIR}/linux"

# Required by DSA
export SteamAppId="211820"
export SteamGameId="211820"
export DSA_ORIG_STEAM_API_LIB="${SB_LIB64}/libsteam_api.so"

. "${DSA_DIR}/common.sh"
. "${DSA_DIR}/config.sh"

echo -e "\033[1;92mLaunching Starbound with DummySteamAPI...\033[0m"

cd "${SB_DIR}"

export LD_LIBRARY_PATH="${SB_LIB64}:${LD_LIBRARY_PATH}"
export LD_PRELOAD="${DSA_DIR}/bin/libsteam_api.so:${LD_PRELOAD}"

exec "${SB_BIN64}"
