#!/bin/bash

set -e

DSA_DIR=$(realpath $(dirname "$0"))

ETG_DIR="${HOME}/.steam/steam/steamapps/common/Enter the Gungeon"
ETG_BIN32="${ETG_DIR}/EtG.x86"
ETG_LIB32="${ETG_DIR}/EtG_Data/Plugins/x86"

# Required by DSA
export SteamAppId="311690"
export SteamGameId="311690"
export DSA_ORIG_STEAM_API_LIB="${ETG_LIB32}/libsteam_api.so"

export CFLAGS="-m32"
export LDFLAGS="-m32"

. "${DSA_DIR}/common.sh"
. "${DSA_DIR}/config.sh"

echo -e "\033[1;92mLaunching Enter the Gungeon with DummySteamAPI...\033[0m"

cd "${ETG_DIR}"

export LD_LIBRARY_PATH="${ETG_LIB32}:${LD_LIBRARY_PATH}"
export LD_PRELOAD="${DSA_DIR}/bin/libsteam_api.so:${LD_PRELOAD}"

exec "${ETG_BIN32}"
